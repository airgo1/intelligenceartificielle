package agent;

import agent.capteur.Capteur;
import agent.capteur.CapteurObstacle;

import java.util.ArrayList;

/**
 * @author Cédric Baloche
 * @version 0.1
 * @since 23/04/2019
 */
public class AgentSimple extends AgentGenerique {
    // L'agent se déplace aléatoirement jusqu'à trouver la cible
    // Puis il devra tourner autour de la cible

    public AgentSimple(String identifiant) {
        super(identifiant);
        addCapteur(new CapteurObstacle("1", "N",1, identifiant));
        addCapteur(new CapteurObstacle("2", "O",1, identifiant));
        addCapteur(new CapteurObstacle("3", "S",1, identifiant));
        addCapteur(new CapteurObstacle("4", "E",1, identifiant));
    }


}
