package agent.capteur;

public class CapteurObstacle extends Capteur {


    public Boolean getDonnee() {

        return true;
    }

    public CapteurObstacle(String nom, String orientation, int portee, String identifiantProprietaire) {
        super(nom, orientation, portee, identifiantProprietaire);
    }
}
