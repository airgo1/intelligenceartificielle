package agent.capteur;

import environnement.MapCommunicator;

public abstract class Capteur {

    private String nom;
    private String orientation;
    private int portee;
    private String identifiantProprietaire;

    public Capteur(String nom, String orientation, int portee, String identifiantProprietaire)
    {
        this.nom = nom;
        this.orientation = orientation;
        this.portee = portee;
        this.identifiantProprietaire = identifiantProprietaire;
    }
    /**
     * La méthode doit récupérer la position de l'agent pour déterminer l'information à renvoyer
     * @return
     */
    abstract Boolean getDonnee();
}
